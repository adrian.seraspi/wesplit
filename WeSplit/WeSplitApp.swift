//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Adrian Jun Seraspi on 4/24/24.
//

import SwiftUI

@main
struct WeSplitApp: App {
  var body: some Scene {
    WindowGroup {
      ContentView()
    }
  }
}
