//
//  ContentView.swift
//  WeSplit
//
//  Created by Adrian Jun Seraspi on 4/24/24.
//

import SwiftUI

struct ContentView: View {
  let tipPercentages = [10, 15, 20, 25, 0]
  
  @State private var checkAmount = 0.0
  @State private var numberOfPeople = 2
  @State private var tipPercentage = 20
  
  @FocusState private var amountIsFocused: Bool
  
  var body: some View {
    NavigationStack {
      Form {
        
        Section {
          TextField(
            "Amount",
            value: $checkAmount,
            format: .currency(code: Locale.current.currency?.identifier ?? "USD")
          )
          .keyboardType(.decimalPad)
          .focused($amountIsFocused)
        }
        
        Section {
          Text(
            totalPerPerson,
            format: .currency(code: Locale.current.currency?.identifier ?? "USD")
          )
        }
        
        Section("How much tip do you want to leave?") {
          Picker("Tip percentage", selection: $tipPercentage) {
            ForEach(tipPercentages, id: \.self) {
              Text($0, format: .percent)
            }
          }.pickerStyle(.segmented)
        }
        
        Picker("Number of people", selection:  $numberOfPeople) {
          ForEach(2..<100) {
            Text("\($0) people")
          }
        }.pickerStyle(.navigationLink)
      }
      .navigationTitle("WeSplit")
      .navigationBarTitleDisplayMode(.inline)
      .toolbar {
        if amountIsFocused {
          Button("Done") {
            amountIsFocused = false
          }
        }
      }
    }
      
  }
}

extension ContentView {
  var totalPerPerson: Double {
    let peopleCount = Double(numberOfPeople + 2)
    let tipSelection = Double(tipPercentage)
    
    let tipValue = checkAmount / 100 * tipSelection
    let grandTotal = checkAmount + tipValue
    let amountPerPerson = grandTotal / peopleCount
    
    return amountPerPerson
  }
}

#Preview {
  ContentView()
}
